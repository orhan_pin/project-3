# 2/22/2021

- استفاده از Owl Carousel برای نمایش مقالات بصورت اسلاید (AutoPlay)

# 2/24/2021

- دو زبانه کردن سایت

- استفاده از django-translated-fields برای ترجمه کردن فیلد مدل ها

- deploy kardan rooye pythonanywhere.com : http://ph00.pythonanywhere.com/
