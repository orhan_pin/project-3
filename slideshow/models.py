from django.db import models
from django.utils import timezone
from translated_fields import TranslatedField
from django.utils.translation import gettext_lazy as _


class Article(models.Model):
    title = TranslatedField(models.CharField(verbose_name=_('title'), max_length=200, default="1"))
    image = models.ImageField(verbose_name=_('image'), upload_to="images")
    status = models.BooleanField(verbose_name=_('status'), default=False)
    publish = models.DateTimeField(verbose_name=_('publish'), default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['publish']
        verbose_name = _("Article")
        verbose_name_plural = _("Articles")

    def __str__(self):
        return self.title

    def entitle(self):
        return self.title_en

    entitle.short_description = "English Title"

    def fatitle(self):
        return self.title_fa

    fatitle.short_description = "Farsi Title"


class SlideShow(models.Model):
    article = models.OneToOneField(Article, on_delete=models.CASCADE, verbose_name=_('Article'))
    status = models.BooleanField(verbose_name=_('status'), default=False)

    class Meta:
        ordering = ['article__publish']
        verbose_name = _("SlideShow")
        verbose_name_plural = _("SlideShows")

    def __str__(self):
        return self.article.title
