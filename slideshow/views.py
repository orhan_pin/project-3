from django.shortcuts import render, redirect
from django.utils.translation import activate


def home(request):
    # activate('fa')
    return render(request, 'home.html')


def change_lang(request):
    activate(request.GET.get('lang'))
    return redirect(request.GET.get('next'))
