# Generated by Django 3.1.5 on 2021-02-24 11:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('slideshow', '0003_auto_20210223_0028'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='article',
            options={'ordering': ['publish']},
        ),
        migrations.AlterModelOptions(
            name='slideshow',
            options={'ordering': ['article__publish']},
        ),
        migrations.RemoveField(
            model_name='article',
            name='title',
        ),
        migrations.AddField(
            model_name='article',
            name='title_en',
            field=models.CharField(default='0', max_length=200),
        ),
        migrations.AddField(
            model_name='article',
            name='title_fa',
            field=models.CharField(default='0', max_length=200),
        ),
    ]
