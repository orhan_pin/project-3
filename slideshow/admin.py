from django.contrib import admin
from .models import SlideShow, Article

admin.site.register(SlideShow)


class ArticleAdminPanel(admin.ModelAdmin):
    list_display = ('entitle', "fatitle", 'status')
    list_display_links = ['entitle']


admin.site.register(Article, ArticleAdminPanel)
